---
title: Pass by value & Pass by Reference
date: 2023-03-02 15:36:27
tags:
---

In JavaScript, there are two ways to pass an argument while calling a function, either Pass by value or Pass by reference.

## Pass By Reference

In Pass by Reference, the reference/address of the variable is passed to the calling function as arguments. As a result of reference being passed, if any changes are made to the argument inside the function, then the variable outside the function also changes since they share the same address. JavaScript uses pass by reference for non-primitive datatypes such as objects, arrays, and functions.

```js

  //object: non-primitive datatype -> pass by refernce
let item = { key: 2 };
console.log("Value before calling callByValue Function", item);

function callByValue(val) {
  val["keyNew"] = 1;
  delete val["key"];
  console.log("Value inside  callByValue Function", item);
}
callByValue(item);
console.log("Value after calling callByValue ", item);


Output:

Value before calling callByValue Function: { key: 2 }
Value inside  callByValue Function: { keyNew: 1 }
Value after calling callByValue:  { keyNew: 1 }
```

<hr>

## Pass By Value

In Pass by Value, the value of the variable is copied to the calling function as arguments. As a result, any changes made to the argument inside the function do not change the variable outside the function as they do not share the same address. JavaScript uses pass by value for primitive-datatype such as number, string, null and boolean.

```js
//string: primitive datatype -> pass by value
let item = "hi";
console.log("Value before calling callByValue Function", item);

function callByValue(val) {
  val = val + " " + "abhay";
  console.log("Value inside  callByValue Function", item);
}
callByValue(item);
console.log("Value after calling callByValue ", item);


Output:

Value before calling callByValue Function hi
Value inside  callByValue Function hi
Value after calling callByValue  hi
```

<hr>

### Why does JavaScript uses pass by value and not pass by reference for primitive datatypes such as number and string ?

The question to this answer lies in the nature of datatype of number and string, which is primitive. There are two types of datatypes in Javascript which are primitive and non-primitive. Number, string, boolean, null, undefined, symbol fall under the category of primitive datatypes, while arrays, objects, and functions fall under the category of non-primitive datatypes. The main point of difference between the both is that primitive datatypes are immutable, meaning once created, they cannot be changed, while non-primitive can be changed after getting created.

Whenever we reassign a value to a variable holding string or number value what happens under the hood is that a new temporary variable is created and the original value from the original variable is passed to the temporary with the specified operations, the temporary variable now holds the new/changed value and the variable name now points to the temporary variable address location and the original variable is sent for garbage collection.

All this is done because, by their nature primitive datatypes are immutable and their value cannot be changed therefore we have to create a copy/temporary variable. Since primitive datatypes are immutable, therefore they cannot be passed as a reference to the function. Essentially there are no address pointers available in ECMA script to reference primitive values.

<hr>

### Why does JavaScript uses pass by reference and not pass by value for non-primitive datatypes such as arrays and functions?

Since objects and arrays are non-primitive datatypes, they are mutable meaning their value can be changed even after getting created. So there is no need to create an additional variable for copying value, which generally is a wastage of memory.

So, whenever a an array or object is passed in the function arguments , the function argument hold the address same as that of the variable outside the function and any changes made in the argument are reflected in the variable outside the function. All the non-primitive data types interact by the reference, so when we set their values equal to each other or pass them to a function, then they all point to the same memory space (address).
