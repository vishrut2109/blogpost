---
title: Truthy & Falsy Values
date: 2023-02-22 15:25:02
tags:
---

In simple terms, truthy is something that evaluates to true and falsey is something that evaluates to false. JavaScript evaluates a value into true or false by using type coercion.

<br>

## Falsey Values

Given below are the values that are considered as falsey values in javascript.

<br>

| Value            | Description                     |
| ---------------- | ------------------------------- |
| false            | keyword false                   |
| null             | no value present                |
| undefined        | variable with no value assigned |
| NaN              | not a number                    |
| 0 or 0.0 or 0\*0 | zero number                     |
| -0               | negative zero                   |
| 0n               | big int zero                    |
| "", '', ``       | empty string                    |

<br>

Some examples of falsey values are given below:

```js
console.log(Boolean(false));
// output-> false

console.log(Boolean(null));
// output-> false

console.log(Boolean(0n));
// output-> false

console.log(Boolean(-0));
// output-> false

console.log(Boolean(""));
// output-> false
```

## Truthy Values

All values that are not falsey values are truthy values. The easiest way to check if something is truthy is to see that it’s not falsey. Examples of truthy values are given below:

```js
console.log(Boolean(true));
// output-> true

console.log(Boolean({}));
// output-> true

console.log(Boolean(88));
// output-> true

console.log(Boolean("0"));
// output-> true

console.log(Boolean("false"));
// output-> true

console.log(Boolean(-7.9));
// output-> true

console.log(Boolean(-Infinity));
// output-> true
```

## Working with Truthy and Falsey Values

In the lines of code given below, we have given value “Bruce Wayne” to variable nameOfBatman.
This line —> if(nameOfBatman) checks if the value of variable is empty or not, if it is found not empty it will be a truthy value and the if block will run.

```js
let nameOfBatman="Bruce Wayne";
function nameOf(nameOfBatman){

if(nameOfBatman){
console.log("the name of Batman is ", nameOfBatman);
} else{
console.log("meaning not found")
}
}
nameOf(nameOfBatman)

//Output
the name of Batman is  Bruce Wayne
```

In the lines of code given below, we have assigned empty string to nameOfBatman.
This line —> if(nameOfBatman) checks if the value of variable is empty or not, here it is found empty hence, it will be a falsey value and the if block will not run.

```js
let nameOfBatman="";
function nameOf(nameOfBatman){

if(nameOfBatman){
console.log("the name of Batman is ", nameOfBatman);
} else{
console.log("meaning not found")
}
}
nameOf(nameOfBatman)

//Output
meaning not found
```

One of the main reason for using truthy and falsey values is that it helps to write clear, concise and readable code.

We have this function printNumberGreaterThan3() that takes as an argument an array that contains numbers and only prints the values that are greater than 3.

```js
list = [1, 2, 3, 4, 5, 6, 7, 8];
function printNumberGreaterThan3(list) {
  if (list.length !== 0) {
    for (let index = 0; index < list.length; index++) {
      if (list[index] > 3) {
        console.log(list[index]);
      }
    }
  } else {
    console.log("the array is empty");
  }
}
printNumberGreaterThan3(list);

// Output
4;
5;
6;
7;
8;
```

Notice this line:

if(list.length!==0)
We can make the condition much more clear with truthy and falsey values:

if (list):

If list is empty, it will be a falsey value, so condition will evaluate to False.

If list is not empty, it will be a truthy value, so condition will evaluate to true.

Here, we get the same functionality with much more clear code.

Final function:

```js
list = [1, 2, 3, 4, 5, 6, 7, 8];
function printNumberGreaterThan3(list) {
  if (list) {
    for (let index = 0; index < list.length; index++) {
      if (list[index] > 3) {
        console.log(list[index]);
      }
    }
  } else {
    console.log("the array is empty");
  }
}
printNumberGreaterThan3(list);

// Output
4;
5;
6;
7;
8;
```
