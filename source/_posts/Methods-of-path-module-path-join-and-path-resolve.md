---
title: "Methods of path module: path.join and path.resolve"
date: 2023-03-15 17:49:57
tags:
---

## path module

The path module in Node.js provides useful methods for working with file and directory paths in a platform-independent way. The Path module has several methods such as path.normalize, path.isAbsolute, path.basename which are used to perform function like normalizing a path, checking if a given path is an absolute path, extract the base name of a file respectively. All this can easily be done with the help of path module methods.

It can be accessed using:

```js
const path = require("node:path");
// here, in 'node:path' node tells that path is a built in module
```

Another advantage of using path module mehtods is it can be used to handle platform-specific path separators. For example, If you're working with paths that use different separators on different platforms (e.g., backslashes on Windows and forward slashes on Unix-like systems), you can use path.resolve() to handle this automatically.

Here we are going to discuss,

- path.join()
- path.resolve()

## path.join()

path.join accepts a sequence of path segments as arguments and joins them using the platform-specific delimiter (i.e. for windows \ while for most Unix based /) then normalizes the resulting path and then returns path value as a string. The path.join() simply concatenates the path strings in the arguments and then may or may not return an absolute path.

```js
const path = require("path");

const dir = path.join("home", "work", "my-project");

console.log(dir);

//Output
home/work/my-project
```

If the joined path string is a zero-length string then '.' will be returned, representing the current working directory.

```js
const path = require('path')

const dir = path.join();

console.log(dir);

//Output
.
```

## path.resolve()

path.resolve() also takes in a sequence of path segments as arguments but will always return an absolute path. There are some cases that may arise when using path.resolve(), and in each of these cases path.resolve() will return an absolute path.

The sequence of paths in input is processed from right to left, with each subsequent path prepended until an absolute path is made.
Here in the code example below, we get an absolute path by prepending the sequence of arguments.

```js
const path = require('path')

const dir = path.resolve('/home', 'work', 'my-project');

console.log(dir);

//Output
/home/work/my-project
```

If, after processing all given path segments, an absolute path has not yet been generated, the current working directory is used to have an absolute path.

```js
const path = require('path')

const dir = path.resolve('home', 'work', 'my-project');

console.log(dir);

// Output
/Users/vishrutsharma/Desktop/home/work/my-project
```

when we have two arguments which are both absolute paths, then the return value will be the rightmost absolute path beacuse it processes arguments from right to left, until an absolute path is created.

```js
const path = require("path");

const dir = path.resolve("/foo/bar", "/tmp/file/");

console.log(dir);
//Output
/tmp/file
```

If no path segments are passed, path.resolve() will return the absolute path of the current working directory.

```js
const path = require("path");

const dir = path.resolve();

console.log(dir);
//Output
/Users/vishrutsharma/Desktop
```
