---
title: Methods Of Promise
date: 2023-03-14 18:51:01
tags:
---

JavaScript is a single-threaded language and synchronous by default, what this means is that JavaScript by default will execute only one line of code at a time. But, this makes JavaScript slow, and in order to overcome this we have asynchronous programming in JavaScript. Asynchronous programming can be handled with the help of callback functions, promises, and async/await. Until 2015, we could do asynchronous programming by using only callback functions. Callback functions had certain cons of using them such as inversion of control and 'callback hell'. To overcome this ECMAScript 2015, also known as ES6, introduced the JavaScript promise.

A promise in JavaScript is similar to a promise in real life. When we make a promise in real life, we mean to say that we are going to do something in the future. Similarily, in JavaScript also when a promise is made it means that some value in the future will be returned that cannot be returned immediately. A Promise has three possible states:

- fulfilled  —  a promise is fulfilled if operation was successful.
- rejected  —  a promise is rejected if operation was unsuccessful.
- pending  —  a promise is pending if it is neither fulfilled nor rejected.

## Promise.all

### parameters

an array of promises

### Return value

- if the array passed is empty, it will return a promise that is already fulfilled.
- when all the promises in the array resolve it will return a asynchronously fulfilled promise which has value of an array of fulfilled promises, in the order of the promises passed, regardless of completion order.
- Asynchronously rejected promise is returned when any of the promises in the given array rejects. The rejection reason is the rejection reason of the first promise that was rejected.

### When to use Promise.all?

Consider a scenario where you have ten promises representing async operations, such as creating/deleting multiple files, network calls or database connections. You need to wait for all the promises to resolve before proceeding. To achieve this, you can use Promise.all by passing all ten promises as arguments. Once all the promises are fulfilled, Promise.all will also be fulfilled. Alternatively, if any of the ten promises are rejected with an error, Promise.all will also be rejected. In the lines of code given below, writeFiles() function returns a promise by making use of Promise.all which accepts an array of promises stored in variable allPromises. Here, all the promises in createEachFile() function are fulfilled and as a result final promise which includes an array of fulfilled promises is passed as a value to .then() which can be seen displayed in the output.

```js
const path = require("path");
const fs = require("fs");
let directoryPath = __dirname;

const promise = writeFiles();
promise
  .then(function (message) {
    console.log(message);
  })
  .catch(function (err) {
    console.error(err);
  });

function writeFiles() {
  return new Promise(function (resolve, reject) {
    let allPromises = [];

    let arr = new Array(5).fill(0);

    allPromises = arr.map((currElement, index) => {
      let fileName = `random${index + 1}.json`;
      return createEachFile(fileName);
    });

    function createEachFile(fileName) {
      return new Promise(function (resolve, reject) {
        fs.writeFile(
          path.join(directoryPath, fileName),
          JSON.stringify({ carName: "taigun", brand: "volkswagen" }),
          "utf8",
          (err) => {
            if (err) {
              reject(err);
            } else {
              resolve(`File ${fileName} is written`);
            }
          }
        );
      });
    }

    Promise.all(allPromises)
      .then(function (response) {
        resolve(response);
      })
      .catch(function (err) {
        reject(err);
      });
  });
}
```

### Output:

[![image](https://www.linkpicture.com/q/output.png)](https://www.linkpicture.com/view.php?img=LPic6410654a58e48147238053)

### Limitations of Promise.all :

While using Promise.all, even if one of the promises gets rejected then Promise.all gets rejected as whole and even if other promises are fulfilled we cannot see thier fulfilled values. There might be a situation where we want to see those fulfilled values of other promises and by using Promise.all we can never retrieve the values of those fulfilled promises.

## Promise.allSettled

### parameters

an array of promises

### Return value

- if the array passed is empty, it will return a promise that is already fulfilled.
- when all the promises in the array resolve it returns an array of objects, each describing the outcome of one promise in the array, in the order of the promises passed.every outcome object has three properties which are:
  - status: can be either fulfilled or rejected.
  - value: The value that the promise was fulfilled with.
  - reason: The reason that the promise was rejected with.

### When to use Promise.allSettled?

To deal with the limitations of Promise.all, Promise.allSettled was introduced. By using Promise.allSettled we can see the results of all promises whether rejected or fulfilled. Promise.allSettled is useful to handle both fulfilled and rejected promises in the same way, without waiting for all the promises to be complete.

Given below is a code example that tries to explain the usage of Promise.allSettled :

```js
cconst promise1 = Promise.resolve("Promise 1 Resolved Successsfuly");
const promise2 = Promise.reject(new Error("error"));
const promise3 = Promise.resolve("Promise 3 Resolved Successsfuly");

Promise.allSettled([promise1, promise2, promise3]).then(function (values) {
  console.log(values);
});

```

### Output:

[![image](https://www.linkpicture.com/q/output-settled.png)](https://www.linkpicture.com/view.php?img=LPic6410654a58e48147238053)

## Promise.any

- The Promise.any() takes an iterable Object, such as an Array of promises as an input. Once a promise is fulfilled, a single promise is returned and the promise is resolved using the value of the promise.

- If all promises reject, then reject by providing errors for all rejects.

Here , we can see Promise 2 in the output, as by definition Promise.any() returns the first resolved only which in this case is promise2 which has a setTimeout of 2 second. promise3 is not displayed in results as it is a rejected promise.

```js
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 1");
  }, 3000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Promise 3");
  }, 1000);
});

Promise.any([promise1, promise2, promise3])
  .then(function (message) {
    console.log(message);
  })
  .catch(function (message) {
    console.log(message);
  });
```

### Output:

[![image](https://www.linkpicture.com/q/output-any.png)](https://www.linkpicture.com/view.php?img=LPic641070d42bd191244081935)

## Promise.race

This method, in simple words as the name suggests organises a race between promises in the input array and whichever promise is resolved or rejected first, in the shortest time is the one that is diplayed in the results.

Here , we can see Promise 3 in the output, as by definition Promise.race() returns the first resolved or rejected which in this case is promise3 which has a setTimeout of 1 second.

```js
const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 1");
  }, 3000);
});
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 2");
  }, 2000);
});
const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Promise 3");
  }, 1000);
});

Promise.race([promise1, promise2, promise3])
  .then(function (message) {
    console.log(message);
  })
  .catch(function (message) {
    console.log(message);
  });
```

### Output:

[![image](https://www.linkpicture.com/q/output-race.png)](https://www.linkpicture.com/view.php?img=LPic641070d42bd191244081935)
