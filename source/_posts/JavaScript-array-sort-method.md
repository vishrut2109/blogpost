---
title: JavaScript array.sort() method
date: 2023-03-07 15:37:08
tags:
---
 
Sorting is the re-arrangement of elements of a collection according to a defined order either ascending or descending. Sorting is very helpful in some scenarios such as search and database operations as it reduces the complexity of the problem to a great extent. 

In JavaScript, an array.sort() method is used to sort an array. The time complexity of the array.sort() method varies depending on the browser. For example, the firefox browser uses the merge sort implementation while the Chrome browser uses the Timsort implementation which has slightly better performance in case of small arrays and otherwise the same. Also, Timsort is a stable algorithm meaning that the elements with the same value retain their order even after the sorting operation has been performed.

By default, the arrays are sorted in ascending order. The sorting operation happens on the same array and no new array is created. To change the order from ascending to descending, a user defined compareFunction() needs to be passed as an argument to the sort() method. This argument is optional and may or may not be passed depending on the use case.


## Sorting String: Explained
JavaScript sorts alphabetically in the same order as it is followed in a dictionary. If two strings are equal, then the shortest one is put first.

```js
const names = ["Harrier", "Polo", "Taigun", "Tiguan", "Slavia", "Vento", "Virtus"];

console.log(names.sort());

// Output: By Default, JavaScript sorts in ascending order
['Harrier', 'Polo', 'Slavia', 'Taigun', 'Tiguan',  'Vento', 'Virtus']
console.log(names.sort().reverse());
// Output: Here, we have the result in descending order by using reverse function. Another way of getting descending order is by using the compareFunction, which is illustrated ahead in the blog.

['Virtus',  'Vento','Tiguan', 'Taigun', 'Slavia', 'Polo', 'Harrier']
```

### Sorting an array of different case strings
If we have an array that contains elements in both uppercase and lowercase, then to sort this array with native sort() method would give incorrect order.
To sort this array alphabetically, we need to use a custom compareFunction to convert all elements to the same case either uppercase or lowercase for comparison and pass that function to the sort() method.

```js
const names = ["Harrier", "polo", "taigun", "Tiguan", "Slavia", "vento", "Virtus"];

names.sort(function (carA, carB) {
    let temp1 = carA.toLowerCase();
    let temp2 = carB.toLowerCase();
     if (temp1 < temp2) {
    return -1;
  }
  if (temp1 > temp2) {
    return 1;
  }
    
  // When carA and carB are equal
  return 0;
});
console.log(names);

// Output: 
[
'Harrier', 'polo', 'Slavia',  'taigun', 'Tiguan',  'vento', 'Virtus'
]

```

## Sorting Integers: using compareFunction(number1, number2)
The compareFunction is an optional parameter in sort() method. If the compareFunction is not passed as the parameter of the sort() method, then it, by default sorts the elements by converting them into strings and comparing strings in UTF-16 code units order. The compareFunction is passed with two values number1, the first element for comparison and number2, the second element for comparison. Sorting is then done based on its return value:

* return value < 0 : number1 is placed before number2.
* return value > 0 : number1 is placed after number2.
* return value = 0 : Keeps the same order as in the original array

```js
let numbers = [70, 10, 5, 25];

// Without compare function
console.log("Without compare function:");
numbers.sort();
console.log(numbers);

// With a compare function
console.log("With a compare function:");
numbers.sort(function(number1, number2){
   if (number1 < number2) {
    return -1;
  }
  if (number1 > number2) {
    return 1;
  }
    
  // When number1 and number2 are equal
  return 0;
});
console.log(numbers);

// Output: 
Without compare function:
[ 10, 25, 5, 70 ]
With a compare function:
[ 5, 10, 25, 70 ]
```

Here, we can observe that we get different outputs for the same array when sort() method is used with and without compareFunction(). The reason for this is while working an with array of numbers, using native sort() method gives in wrong result as native sort() method converts the elements of array into strings and then compares their sequences of UTF-16 code units values. Therefore, we need to use the compareFunction to get the elements sorted correctly.

### Sorting Floating Point Numbers
Floating point numbers can be sorted the same as integers. Changing the order from ascending to descending is very easy and involves replacing 1 with -1. Given below is an example to illustrate the same:

```js
let numbers = [70.12, 10.65, 8.56, 0.75, 13.9];

numbers.sort(function(number1, number2){
   if (number1 < number2) {
    return -1;
  }
  if (number1 > number2) {
    return 1;
  }
    
  // When number1 and number2 are equal
  return 0;
});
console.log("In Ascending Order");
console.log(numbers);


let numbers = [70.12, 10.65, 8.56, 0.75, 13.9];

numbers.sort(function(number1, number2){
   if (number1 < number2) {
    return 1;
  }
  if (number1 > number2) {
    return -1;
  }
    
  // When number1 and number2 are equal
  return 0;
});
console.log("In Descending Order");
console.log(numbers);


// Output: 
In Ascending Order
[ 0.75, 8.56, 10.65, 13.9, 70.12 ]
In Descending Order
[ 70.12, 13.9, 10.65, 8.56, 0.75 ]
```


## Sorting an Array of Objects

Here, we have an array of objects, cars with two properties, 'name' and 'engineBHP'.

```js
const vw = [
  {
    name: 'Polo',
    engineBHP: 103
  },
  {
    name: 'Virtus GT',
    engineBHP: 147.51
  },
  {
    name: 'Taigun',
    engineBHP: 113.98
  },{
    name: 'Tiguan',
    engineBHP: 187.74
  },{
    name: 'Vento',
    engineBHP: 103.2
  },
];

vw.sort((carA, carB) => {
  return  carB.engineBHP - carA.engineBHP ;
});

console.log(vw);

// Output:
[
  { name: 'Tiguan', engineBHP: 187.74 },
  { name: 'Virtus GT', engineBHP: 147.51 },
  { name: 'Taigun', engineBHP: 113.98 },
  { name: 'Vento', engineBHP: 103.2 },
  { name: 'Polo', engineBHP: 103 }
]
```
### Sorting An Array Of Object With Multiple Keys

```js
const vw = [
  {
    name: 'Polo',
    engineBHP: 103,
    torque: 180
  },
  {
    name: 'Virtus GT',
    engineBHP: 147.51,
    torque: 250
  },
  {
    name: 'Slavia ',
    engineBHP: 147.51,
    torque: 249
  },
  {
    name: 'Taigun ',
    engineBHP: 113.98,
    torque: 178
  },{
    name: 'Tiguan ',
    engineBHP: 187.74,
    torque: 320
  },{
    name: 'Kushaq ',
    engineBHP: 113.98,
    torque: 178
  },
];

console.log(vw.sort((carA, carB)=> {
   
  if (carA.engineBHP === carB.engineBHP){ 
    return   carB.torque - carA.torque; 
  } else {
    return carB.engineBHP - carA.engineBHP; 
  }
}))

// Output
[
  { name: 'Tiguan ', engineBHP: 187.74, torque: 320 },
  { name: 'Virtus GT', engineBHP: 147.51, torque: 250 },
  { name: 'Slavia ', engineBHP: 147.51, torque: 249 },
  { name: 'Taigun ', engineBHP: 113.98, torque: 178 },
  { name: 'Kushaq ', engineBHP: 113.98, torque: 178 },
  { name: 'Polo', engineBHP: 103, torque: 180 }
]

```

From the output, we can observe some things:
* First criterion for comparison is engineBHP.
* If we cannot sort with the first criteria alone, then the second criterion of comparison which is torque is used: which acts as a tie-breaker between cars having the same engineBHP. this can be seen in the case of VirtusGT and Slavia.
* The stability of the algorithm is maintained as in the case of Taigun and Kushaq both have the same figures for both torque and engineBHP and their original order is maintained.

## Summary:
* In JavaScript, an array.sort() method is used to sort an array.
* By default, the arrays are sorted in ascending order.
* The sorting operation happens on the same array and no new array is created.
* To sort integers we have to make use of compare function ().
* To sort strings with mixed cases, we have to make use of compareFunction and then convert the string to either uppercase or lowercase and then compare.


